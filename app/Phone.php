<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $fillable = ['label', 'number'];

    public function contacts()
    {
        return $this->belongsToMany('App\Contact');
    }
}
