<?php


namespace App\Helpers;


use App\Document;
use InvalidArgumentException;

trait Utils
{
    public function getFileName(Document $document): string
    {
        return str_replace(' ', '_', $document->name) . '.' . pathinfo($document->path, PATHINFO_EXTENSION);
    }

    public static function searchIn($subject, string $query): bool
    {
        if (is_array($subject)) {
            foreach ($subject as $s) {
                if (self::searchIn($s, $query)) {
                    return true;
                }
            }
            return false;
        } else {
            return false !== strpos(strtolower((string) $subject), strtolower($query));
        }
    }
}
