<?php

namespace App;

use App\Helpers\Utils;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use function MongoDB\BSON\toJSON;

class Document extends Model
{
    use Utils;

    protected $fillable = ['name', 'path'];

    public function procedures()
    {
        return $this->belongsToMany('App\Procedure');
    }

    public static function getSeparatedDocs(Collection $documents)
    {
        return collect([
            'docDownload' => self::getDownload($documents),
            'docNoDownload' => self::getNoDownload($documents)
        ]);
    }

    public static function getDownload(Collection $documents): array
    {
       return $documents->filter(function ($document) {
            return $document->isDownloadable();
        })->toArray();
    }

    public static function getNoDownload(Collection $documents): array
    {
        return $documents->filter(function ($document) {
            return !$document->isDownloadable();
        })->toArray();
    }

    public function isDownloadable(): bool
    {
        return !is_null($this->path);
    }

    public static function search(Collection $documents, string $query): bool
    {
        foreach ($documents as $document) {
            if (self::searchIn($document->name, $query)) {
                return true;
            }
        }
        return false;
    }

}
