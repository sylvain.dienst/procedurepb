<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Helpers\Utils;
use App\Http\Requests\StoreContact;
use App\Phone;
use App\Procedure;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    use Utils;

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreContact $request
     * @return false|string
     */
    public function store(StoreContact $request)
    {
        $contact = new Contact($request->all());
        $contact->save();
        $phones = [];
        foreach (array_combine((array) $request->labels, (array) $request->numbers) as $label => $number) {
            $phones[] = new Phone(['label'=> $label, 'number' => $number]);
        }
        $contact->phones()->saveMany($phones);
        return $contact->load('phones')->toJson();
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return false|string
     */
    public function showPhones($id)
    {
        try {
            $contact = Contact::findOrFail($id);
            return $contact->phones->toJson();
        } catch (\Exception $e) {
            return json_encode(['success' => false]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //TODO
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO
    }
}
