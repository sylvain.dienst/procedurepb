<?php

namespace App\Http\Controllers;

use App\Category;
use App\Contact;
use App\Document;
use App\Helpers\Utils;
use App\Http\Requests\StoreProcedure;
use App\Procedure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;
use PhpParser\Comment\Doc;

class ProcedureController extends Controller
{
    use Utils;

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('procedure/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('procedure/create' , ['categories' => Category::all(), 'documents' => Document::all(), 'contacts' => Contact::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProcedure $request
     * @return RedirectResponse
     */
    public function store(StoreProcedure $request): RedirectResponse
    {
        $procedure = new Procedure(['title'=> $request->title, 'content' => $request->contenu, 'abstract' => $request->abstract]);
        $procedure->user_id = Auth::id();
        $procedure->category_id = $request->category;
        $procedure->save();
        $procedure->documents()->sync($request->documents);
        $procedure->contacts()->sync($request->contacts);
        return redirect()->route('procedure.edit', $procedure->id)->with('success', 'Procédure sauvegardée');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return string
     */
    public function showDocuments($id): string
    {
        try {
            $procedure = Procedure::findOrFail($id);
            return Document::getSeparatedDocs($procedure->documents)->toJson();
        } catch (\Exception $e) {
            return json_encode(['success' => false]);
        }
    }

    public function showContacts($id): string
    {
        try {
            $procedure = Procedure::findOrFail($id)->load('contacts.phones');
            return $procedure->contacts->toJson();
        } catch (\Exception $e) {
            return json_encode(['success' => false]);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return View|string
     */
    public function edit($id)
    {
        try {
            $procedure = Procedure::find($id);
            return view('procedure/edit', [
                'procedure' => $procedure,
                'categories' => Category::all(),
                'documents' => collect([
                    'checkedDocs' => $procedure->documents,
                    'noCheckedDocs' => Document::all()->diff($procedure->documents)
                    ]),
                'contacts' => collect([
                    'checkedContacts' => $procedure->contacts,
                    'noCheckedContacts' => Contact::all()->load('phones')->diff($procedure->contacts)
                ]),
            ]);
        } catch (\Exception $e) {
            return json_encode(['success' => false]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreProcedure $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(StoreProcedure $request, $id)
    {
        $procedure = Procedure::findOrFail($id);
        $procedure->user_id = Auth::id();
        $procedure->category_id = $request->category;
        $procedure->title = $request->title;
        $procedure->save();
        $procedure->documents()->sync($request->documents);
        return redirect()->route('procedure.edit', $procedure->id)->with('success', 'Procédure modifiée');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        Procedure::destroy($id);
        return back()->with('success', 'Procédure supprimée');
    }

    public function search(Request $request, $query)
    {
        if($query == 'empty') {
            return Procedure::all()->load('category', 'documents', 'contacts')->toJson();
        }
        $procedures = Procedure::all()->filter(function ($procedure) use ($query) {
            $procedure->load('category', 'documents', 'contacts');
            return self::searchIn($procedure->toArray(), $query);
        });
        return $procedures->load('category', 'documents', 'contacts')->toJson();
    }
}
