<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Helpers\Utils;
use App\Http\Requests\StorePhone;
use App\Phone;
use Illuminate\Http\Request;

class PhoneController extends Controller
{
    use Utils;

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePhone $request
     * @return false|string
     */
    public function store(StorePhone $request)
    {
        $document = new Phone($request->all());
        $document->save();
        return $document->toJson();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //TODO
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO
    }

}
