<?php

namespace App\Http\Controllers;

use App\Document;
use App\Helpers\Utils;
use App\Http\Requests\StoreDocument;
use App\Procedure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    use Utils;

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreDocument $request
     * @return false|string
     */
    public function store(StoreDocument $request)
    {
        $document = new Document();
        $document->name = $request->name;
        $document->save();
        return $document->toJson();
    }

    /**
     * Store file itself
     * @param Request $request
     * @return mixed
     */
    public function storeFile(Request $request)
    {
        $document = Document::find($request->id);
        $document->path = $request->file('file')->store('documents');
        $document->save();
        return json_encode(['success' => true, 'data' => $document->toArray()]);
    }


    /**
     * Download Document
     * @param $id
     * @return mixed
     */
    public function downloadFile($id)
    {
        $document = Document::find($id);
        return Storage::download($document->path, $this->getFileName($document));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //TODO
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //TODO
    }
}
