<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProcedure extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'category' => 'required',
            'content' => 'nullable',
            'documents' => 'nullable',
        ];
    }

    public function messages(): array
    {
        return [
            'title.required' => 'Le champs titre est requis',
            'category.required' => 'La catégorie est requis'
        ];
    }
}
