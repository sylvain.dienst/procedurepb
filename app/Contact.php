<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['firstname', 'lastname','email', 'company', 'comments', 'address'];

    public function procedures()
    {
        return $this->belongsToMany('App\Procedure');
    }

    public function phones()
    {
        return $this->belongsToMany('App\Phone');
    }
}
