<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procedure extends Model
{
    protected $fillable = ['title', 'content', 'abstract', 'id_user', 'id_category'];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function documents()
    {
        return $this->belongsToMany('App\Document');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function contacts()
    {
        return $this->belongsToMany('App\Contact');
    }
}
