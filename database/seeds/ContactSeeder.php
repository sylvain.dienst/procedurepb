<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $id = DB::table('contacts')->insertGetId([
            'firstname' => 'Jean',
            'lastname' => 'Dupont',
            'email' => 'jean.dupont@gmail.com',
            'company' => 'IRD',
            'address' => '863 avenue jean soulier 34000 Montpellier',
            'comments' => 'S\'adresser a Mme Machin'
        ]);
        for ($i=0; $i < 3; $i++) {
            $idP = DB::table('phones')->insertGetId([
                        'label' => 'phone' . $i,
                        'number' => '060000' . $i
                    ]);
            DB::table('contact_phone')->insert([
                'contact_id' => $id,
                'phone_id' => $idP,
            ]);
        }
    }
}
