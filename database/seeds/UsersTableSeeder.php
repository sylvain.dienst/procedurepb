<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Sylvain",
            'email' => 'sylvain.dienst@gmail.com',
            'password' => Hash::make('sylvain1208'),
        ]);
    }
}
