<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DocumentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('documents')->insert([
            'name' => "Premier document",
            'path' => null
        ]);
        DB::table('documents')->insert([
            'name' => "Deuxieme document",
            'path' => null
        ]);
        DB::table('documents')->insert([
            'name' => "Troisieme document",
            'path' => null
        ]);
    }
}
