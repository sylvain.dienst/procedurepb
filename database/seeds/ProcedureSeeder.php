<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProcedureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 5; $i++) {

            $id = DB::table('procedures')->insertGetId([
                'title' => "Premiere procedure " . $i,
                'content' => '<h1>Mon contenu titre</h1><ul><li>Ma</li><li>liste</li><li>a</li><li>puce</li></ul>',
                'abstract' => "Cette procedure est utilise pour ci et pour ca",
                'user_id' => 1,
                'category_id' => 1
            ]);
            DB::table('document_procedure')->insert([
                'procedure_id' => $id,
                'document_id' => 1,
            ]);
            DB::table('document_procedure')->insert([
                'procedure_id' => $id,
                'document_id' => 2,
            ]);
            DB::table('document_procedure')->insert([
                'procedure_id' => $id,
                'document_id' => 3,
            ]);
        }
            $id = DB::table('procedures')->insertGetId([
                'title' => "Deuxieme procedure_" . $i,
                'content' => '<h1>Mon contenu titre</h1><ul><li>Ma</li><li>liste</li><li>a</li><li>puce</li></ul>',
                'abstract' => "Cette procedure est utilise pour ci et pour ca",
                'user_id' => 1,
                'category_id' => 1
            ]);
            DB::table('document_procedure')->insert([
                'procedure_id' => $id,
                'document_id' => 2,
            ]);
            DB::table('procedures')->insert([
                'title' => "Troisieme procedure_" . $i,
                'content' => '<h1>Mon contenu titre</h1><ul><li>Ma</li><li>liste</li><li>a</li><li>puce</li></ul>',
                'abstract' => "Cette procedure est utilise pour ci et pour ca",
                'user_id' => 1,
                'category_id' => 1
            ]);

            DB::table('contact_procedure')->insert([
                'procedure_id' => $id,
                'contact_id' => 1
            ]);

    }
}
