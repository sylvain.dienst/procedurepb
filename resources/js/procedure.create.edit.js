import {getErrors} from "./helpers";

$(document).ready(() => {
    // const peke = require('pekeupload');

    /* -----------------------Category------------------- */

    $('#addNewCat').click(function (event) {
        event.preventDefault();
        $('#createCategory').show();
    });

    $('#saveNewCategory').click(function (event) {
        event.preventDefault();
        const fontA = $('#saveNewCategory');
        const spinner = $('#spinnerCategory');
        const catName = $('#catName');
        fontA.hide();
        spinner.show();
        $.post("/category/", {name: catName.val()})
            .done((response) => {
                $('#category').append(`<option value="${response}" selected>${catName.val()}</option>`);
                fontA.show();
                spinner.hide();
                $('#createCategory').hide();
            })
            .fail((response, textStatus, error) => {
                const errors = getErrors(response);
                fontA.show();
                spinner.hide();
                catName.addClass('is-invalid');
                $('#catNameError strong').html(errors.name[0]);
            })
    });

    /* -------------------Documents--------------------- */
    let myDropzone = null;
    $('#saveDocumentName').on('click', function (event) {
        event.preventDefault();
        $('#saveDocumentName').hide();
        const spinner = $('#spinnerDocument');
        spinner.show();
        $.post("/document", {name: $('#documentName').val()})
            .done((response) => {
                response = JSON.parse(response);
                $('#documentNameGroup').hide();
                $('#documentFileGroup').show();
                spinner.hide();
                $('#saveDocumentName').show();
                $('#documentForm').append(`<input type="hidden" value="${response.id}" name="id"/>`);
                if ($('.dropzone').dropzone) {
                    myDropzone.options.url = `/document/${response.id}/file`;
                } else {
                    const Dropzone = require('dropzone');
                    Dropzone.autoDiscover = false;
                    const CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                    myDropzone = new Dropzone('div#documentDropzone', {
                        url: `/document/${response.id}/file`,
                        method: 'POST',
                        parallelUpload: 1,
                        createImageThumbnails: false,
                        dictDefaultMessage: 'Faites glisser ici les documents ou cliquer sur la zone',
                        dictFallbackMessage: 'Votre navigateur n\'est pas supporté : nous vous invitons à passer sur un navigateur internet moderne...',
                        dictFileTooBig: 'La taille maximale pour un fichier est fixée à {{maxFileSize}}...',
                        dictResponseError: 'Le serveur a renvoyé une erreur de type {{statusCode}}...',
                        dictFileSizeUnits: {
                            tb: 'To', gb: 'Go', mb: 'Mo', kb: 'Ko', b: 'Octet(s)',
                        },
                        headers: {
                            'x-csrf-token': CSRF_TOKEN,
                        },
                        success: (file, responseText) => {
                            addDocumentCheckBox(responseText.data);
                            $('#documentName').val('');
                            $('#documentNameGroup').show();
                            $('#documentFileGroup').hide();
                            $('#documentFile').show();
                            spinner.hide();
                            Flash.success("Document sauvé");
                        },
                        error: (file, responseText) => {
                            // Flash('Fichier ' + file.name + ' non reçu : ' + responseText, 'danger');
                        },
                    });
                    myDropzone.on("complete", function(file) {
                        myDropzone.removeFile(file);
                    });
                }
                // myDropzone.options.url = (files) => {
                //
                // }
            })
            .fail(() => {
                spinner.hide();
                $('#saveDocumentName').show();
                $('#documentName').addClass('is-invalid');
            })
    });

    $('#noDocumentFile').click(function (event) {
        event.preventDefault();
        const docName = $('#documentName');
        const document = {
            id: $('#documentForm').find('input[name="id"]').val(),
            name: docName.val(),
            path: ''
        };
        addDocumentCheckBox(document);
        $('#documentNameGroup').show();
        $('#documentFileGroup').hide();
        docName.val('');
        Flash.success("Document sauvé");
    });

    tinymce.init({
        selector: "#editor",
        height: 500,
        menubar: false,
        initialValue: "<p>This is the initial content of the editor</p>",
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount'
        ],
        toolbar:
            'undo redo | formatselect | bold italic backcolor forecolor| \
            alignleft aligncenter alignright alignjustify | \
            bullist numlist outdent indent | removeformat | help'
    });

    function addDocumentCheckBox(document)
    {
        const color = document.path.length > 0 ? 'text-primary' : '';
        $('#documentsCheck div:first').append(
            `<div class="custom-control custom-checkbox">
            <input class="custom-control-input" name="documents[]" type="checkbox" checked id="document_${document.id}" value="${document.id}">
            <label class="custom-control-label ${color}" for="document_${document.id}">${document.name}</label>
        </div>`
        );
    }

    /* ----------------------Contact-------------------- */

    $('#addNumber').click(function (event) {
        event.preventDefault();
        $('#phoneForm').show();
        $('#buttonAddPhone').hide();
    });

    $('#saveNewPhone').click((event) => {
        event.preventDefault();
        const number = $('#number');
        const label = $('#label');
        const form = $('#newContactForm');
        form.append(`<input type="hidden" name="labels[]" value="${label.val()}"/>`);
        form.append(`<input type="hidden" name="numbers[]" value="${number.val()}"/>`);
        $('#numberAdded').append(`<div class="row"><div class="col-md-4"></div><div class="col-md">
                                    <div class="row">
                                        <div class="col-md">
                                            ${number.val()}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md text-muted">
                                            ${label.val()}
                                        </div>
                                    </div>
                                </div></div>`);
        label.val('');
        number.val('');
    });

    $('#newContactForm').submit((event) => {
        event.preventDefault();
        const form = $('#newContactForm');
        console.log(form.serializeArray());
        $.post('/contact', form.serialize())
            .done((response) => {
                response = JSON.parse(response);
                form.find('input[type=hidden]').remove();
                form.find('input[type=text], input[type=email], textarea').val('');
                form.find('#numberAdded .row').remove();
                $('#phoneForm').hide();
                $('#buttonAddPhone').show();
                $('#contacts div:first').append(` <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" name="contacts[]" type="checkbox" id="response.id" value="${response.id}" checked>
                                        <label class="custom-control-label" for="${response.id}">
                                            ${response.lastname} ${response.firstname}
                                        </label>
                                    </div>`);
                Flash.success("Contact sauvé");
            })
    })

});

