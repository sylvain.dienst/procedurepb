/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/SelectCategory.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('select-category', require('./components/SelectCategory.vue').default);
// Vue.component('add-document', require('./components/AddDocument.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
// const app = new Vue({
//     el: '#app',
// });

(function($){
    Flash = {}
    Flash.success = function(msg, time){
        time = time || 5000;
        $('#flash-container')[0].innerHTML = "<div class='success message'>" + msg + "</div>";
        $('#flash-container').addClass('showing');
        setTimeout(function(){
            $('#flash-container').removeClass('showing');
        }, time);
    };
})(jQuery)


