display();
$(document).ready(() => {
    // Ecoute de la saisie sur le champ de recherche, 500 ms délai attente après dernière saisie
    $('#searchInput').on('keyup', debounce(() => {
        queryWS();
    }, 500));

    $('#showDocuments').click((event) => {
        event.preventDefault();
        if ($('#documents').is(':hidden')) {
            $('#documents').show();
            $('#showDocuments span').attr('class', 'fas fa-folder-open');
        } else {
            $('#documents').hide();
            $('#showDocuments span').attr('class', 'fas fa-folder');
        }

    });
    $('#showContacts').click((event) => {
        event.preventDefault();
        if ($('#contacts').is(':hidden')) {
            $('#contacts').show();
        } else {
            $('#contacts').hide();
        }
    });
});

function addSpinner() {
    $('#listProcedure').hide()
    $('#spinner').show()
}

function stopSpinner() {
    $('#spinner').hide()
    $('#listProcedure').show()
}

function buildList(docs) {
    let list = '';
    $.each(docs, function (idx, val) {
        if (val.path) {
            list += `<a href="/document/${val.id}/file" class="list-group-item list-group-item-action list-group-item-primary"  data-toggle="tooltip" data-placement="top" title="Télécharger le document">
                        <b>${val.name}</b>
                    </a>`
        } else {
            list += `<li class="list-group-item list-group-item-dark">
                    ${val.name}
                </li>`
        }
    });
    return list;
}

function buildContactList(contacts) {
    let list = '';
    $.each(contacts, function (idx, val) {
        list += `<li><a href="" id="contact_${val.id}">${val.lastname}</a></li>`
    })
    return list;
}

function buildContact(contact) {
    $('#fullName').html(`${contact.firstname} ${contact.lastname}`);
    $('#company').html(contact.company);
    $('#address').html(contact.address);
    $('#phones').html('');
    $.each(contact.phones, (idx, val) => {
        $('#phones').append(`<div class="mb-1"><span class="mr-4 text-muted">${val.label}</span><span>${val.number}</span></div>`)
    })
    $('#email').attr('href', `mailto:${contact.email}`);
    $('#email span').html(contact.email);
    $('#comments').html(contact.comments);
}

function showModal(proc) {
    $('#titleProcedure').html(proc.title);
    $('#bodyProcedure').html(proc.content);
    $('#documents').hide();
    $('#showDocuments span').attr('class', 'fas fa-folder');
    $('#contacts').hide();
    $('#docDownload').html('');
    $('#docNoDownload').html('');
    $('#noDocument').hide();
    $('#documentsList').show();
    $('#editProcedure').click(function () {
        window.location = `procedure/${proc.id}/edit`;
    })
    $('#deleteProcedure').click((event) => {
        event.preventDefault();
        if (confirm('Supprimer la procédure ?')) {
            $('#formDelete').attr('action', `procedure/${proc.id}`).submit();
        }
    })
    $.get(`procedure/${proc.id}/documents`)
        .done((docs) => {
            docs = JSON.parse(docs);
            $('#docDownload').html(buildList(docs.docDownload, proc.id));
            $('#docNoDownload').html(buildList(docs.docNoDownload, proc.id));
            if ($('#docDownload').html() === "" && $('#docNoDownload').html() === "") {
                $('#noDocument').show();
                $('#documentsList').hide();
            }
        })
    $('#category').html(proc.category.name);
    $.get(`procedure/${proc.id}/contacts`)
        .done((contacts) => {
            contacts = JSON.parse(contacts);
            if (contacts.length === 0) {
                $('#contactsList').hide();
                $('#noContacts').show();
            } else {
                $('#contactsList').show();
                $('#noContacts').hide();
                $('#contacts ul').html(buildContactList(contacts))
                $.each(contacts, (idx, val) => {
                    $(`#contact_${val.id}`).click((event) => {
                        event.preventDefault();
                        buildContact(val);
                        $('#contactCard').modal();
                    })
                })
            }

        });
    $('#procedureShow').modal();
}

function queryWS() {
    const query = !$('#searchInput').val() ? 'empty' : $('#searchInput').val();
    const url = 'procedure/search/' + query;
    addSpinner();
    // Requête AJAX
    $.ajax({
        url: url,
        type: 'GET',
        cache: false,
        contentType: false,
        processData: false,
        success: (response) => {
            $('#listProcedure').html('');
            // Si aucun résultat, message explicite
            if (response.length === 0) {
                //TODO
            } else {
                $.each(JSON.parse(response), function (idx, val) {
                    $('#listProcedure').append(htmlCard(val));
                    $(`#${val.id}`).click( function (e) {
                        e.preventDefault();
                        showModal(val)
                    })
                })
            stopSpinner();
            }
        },
    });
}

// Réaffiche la page selon le tri choisi
function display() {
    queryWS();
}
//
// // Renvoie vers la page choisie
// export function charge(myIdApp, urlApp, typeApp) {
//     const fd = new FormData();
//     fd.append('idapp', myIdApp);
//
//     // Requête AJAX enregistrement click
//     $.ajax({
//         url: '/menu/click',
//         type: 'POST',
//         data: fd,
//         cache: false,
//         processData: false,
//         contentType: false,
//         // beforeSend: () => {
//         //   $('#ictempo').html('&nbsp;&nbsp;<i class="fa fa-spinner fa-spin"></i>');
//         // },
//         success: (response) => {
//             window.open(urlApp, '_blank');
//         },
//     });
// }

// Construction de la carte de chaque application
function htmlCard(proc) {
    return (`
        <a href="" class="procedure" data-toggle="tooltip"  data-placement="bottom" title="${proc.title}">
            <div class="card text-center ombreover mb-4" id="${proc.id}">
                <div class="card-header">
                    <h5 class="text-center text-truncate">${proc.title}</h5>
                </div>
                <div class="card-body">
                    <p class="card-text p-2 text-black-50">${proc.abstract}</p>
                    <p class="card-text text-right">
                        <small class="font-weight-bold text-danger font-italic">${proc.category.name}</small>
                    </p>
                </div>
                <div style="display: none" id="content">${proc.content}</div>
            </div>
        </a>`);
}

function debounce(callback, wait) {
    let timeout;
    return (...args) => {
        const context = this;
        clearTimeout(timeout);
        timeout = setTimeout(() => callback.apply(context, args), wait);
    };

}
