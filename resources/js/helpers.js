export const getErrors = (response) => {
    return JSON.parse(response.responseText).errors;
}
