@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-5">
              @yield('form_procedure')
            </div>
            <div class="col-md-2">
                <div class="row mb-5">
                    <div class="col">
                        <div class="accordion" id="addAccordion">
                            <div class="card">
                                <div class="card-header" id="headingOneTwo">
                                    <h2 class="mb-0">
                                        <button style="font-size: 0.7em"  class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOneTwo" aria-expanded="true" aria-controls="collapseOneTwo">
                                            <i class="far fa-folder mr-3"></i>Ajouter un document
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseOneTwo" class="collapse show" aria-labelledby="headingOneTwo" data-parent="#addAccordion">
                                    <div class="card-body">
                                        <div id="documentNameGroup" class="row">
                                            <form class="form-inline" method="POST" action="/document" id="documentForm">
                                                @csrf
                                                <div class="form-group mx-sm-3 mb-2">
                                                    <label for="documentName" class="sr-only">Nom</label>
                                                    <input type="text" class="form-control" id="documentName" placeholder="Nom du document">
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" id="saveDocumentName" class="btn btn-primary mb-2">Suivant</button>
                                                    <div class="spinner-border" id="spinnerDocument" style="display: none" role="status">
                                                        <span class="sr-only">Loading...</span>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div id="documentFileGroup" style="display: none" class="row mb-4">
                                            <form action="" method="POST" class="col-12">
                                                <div class="row mb-4">
                                                    <div class="col">
                                                        <div class="dropzone dragDrop mx-auto" id="documentDropzone">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row text-center">
                                                    <div class="col-6">
                                                        <button type="submit" id="documentFile" class="btn btn-primary">
                                                            Envoyer
                                                        </button>
                                                    </div>
                                                    <div class="col-6">
                                                        <button type="submit" id="noDocumentFile" class="btn btn-success">
                                                            Passer
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwoTwo">
                                    <h2 class="mb-0">
                                        <button style="font-size: 0.7em"  class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwoTwo" aria-expanded="false" aria-controls="collapseTwoTwo">
                                            <i class="far fa-address-book mr-3"></i>Ajouter un contact
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwoTwo" class="collapse" aria-labelledby="headingTwoTwo" data-parent="#addAccordion">
                                    <div class="card-body">
                                        <form id="newContactForm" action="" method="POST">
                                            @csrf
                                            <div class="form-group row">
                                                <label for="firstname" class="col-md-4 col-form-label text-md-right">Prénom</label>
                                                <div class="col-md-8">
                                                    <input type="text" id="firstname" class="form-control" name="firstname" >
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="lastname" class="col-md-4 col-form-label text-md-right">Nom</label>
                                                <div class="col-md-8">
                                                    <input type="text" id="lastname" class="form-control" name="lastname" >
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
                                                <div class="col-md-8">
                                                    <input type="email" id="email" class="form-control" name="email" >
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="company" class="col-md-4 col-form-label text-md-right">Société</label>
                                                <div class="col-md-8">
                                                    <input type="text" id="company" class="form-control" name="company" >
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="address" class="col-md-4 col-form-label text-md-right">Adresse</label>
                                                <div class="col-md-8">
                                                    <textarea id="address" class="form-control" name="address" ></textarea>
                                                </div>
                                            </div>
                                            <div id="numberAdded">
                                            </div>
                                            <div class="form-group row" id="buttonAddPhone">
                                                <div class="col">
                                                    Ajouter un numéro
                                                </div>
                                                <div class="col">
                                                    <button id="addNumber" type="button" class="close float-left text-primary" data-toggle="tooltip"  data-placement="bottom" title="Liste des documents">
                                                        <span aria-hidden="true" class="fas fa-plus"></span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="form-row h-100" id="phoneForm" style="display: none">
                                                <div class="form-group col-md-5">
                                                    <label for="label">Libellé</label>
                                                    <input type="text" class="form-control" name="label" id="label" placeholder="Portable">
                                                </div>
                                                <div class="form-group col-md-6">
                                                    <label for="number">Numéro</label>
                                                    <input type="text" name="number" class="form-control" id="number" placeholder="06........">
                                                </div>
                                                <div class="form-group col-md-1 my-auto">
                                                    <a href="" id="saveNewPhone">
                                                        <i class="fas fa-plus fa-2x"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="comments" class="col-md-4 col-form-label text-md-right">Commentaires</label>
                                                <div class="col-md-8">
                                                    <textarea id="comments" class="form-control" name="comments" ></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row text-center">
                                                <div class="col">
                                                    <button type="submit" id="contactSubmit" class="btn btn-primary">
                                                        Envoyer
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
