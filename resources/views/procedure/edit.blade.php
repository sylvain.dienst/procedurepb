@extends('procedure.form_procedure')

@section('procedure.title')
    <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ $procedure->title }}" required autocomplete="title" autofocus>
@endsection

@section('procedure.category')
    <select id="category" class="form-control @error('category') is-invalid @enderror" name="category" required>
        <option></option>
        <option id="addNewCat">Ajouter une nouvelle categorie</option>
        @foreach($categories as $category)
            <option @if($category->id == $procedure->category->id) selected @endif value="{{$category->id}}">{{$category->name}}</option>
        @endforeach
    </select>
@endsection

@section('procedure.abstract')
    <textarea class="form-control" id="abstract" name="abstract" placeholder="Court resumé de la procedure" rows="3">{{$procedure->abstract}}</textarea>
@endsection

@section('procedure.content')
    <input value="{{$procedure->content}}" type="text" name="contenu" id="editor" />
@endsection

@section('procedure.documents')
    <div class="col-6 mb-1">
        @foreach($documents['checkedDocs'] as $docCheck)
            <div class="custom-control custom-checkbox">
                <input name="documents[]" class="custom-control-input" checked type="checkbox" id="{{$docCheck->name}}" value="{{$docCheck->id}}">
                <label class="custom-control-label {{$docCheck->path ? 'text-primary' : ''}}" for="{{$docCheck->name}}">{{$docCheck->name}}</label>
            </div>
        @endforeach
    </div>
    <div class="col-6 mb-1">
        @foreach($documents['noCheckedDocs'] as $docNoCheck)
            <div class="custom-control custom-checkbox">
                <input name="documents[]" class="custom-control-input" type="checkbox" id="{{$docNoCheck->name}}" value="{{$docNoCheck->id}}">
                <label class="custom-control-label {{$docNoCheck->path ? 'text-primary' : ''}}" for="{{$docNoCheck->name}}">{{$docNoCheck->name}}</label>
            </div>
        @endforeach
    </div>
@endsection

@section('procedure.contact')
    <div class="col-6 mb-1">
        @foreach($contacts['checkedContacts'] as $contactCheck)
            <div class="custom-control custom-checkbox">
                <input name="contacts[]" class="custom-control-input" checked type="checkbox" id="{{$contactCheck->lastname}}" value="{{$contactCheck->id}}">
                <label class="custom-control-label" for="{{$contactCheck->id}}">{{$contactCheck->lastname}}</label>
            </div>
        @endforeach
    </div>
    <div class="col-6 mb-1">
        @foreach($contacts['noCheckedContacts'] as $contactNoCheck)
            <div class="custom-control custom-checkbox">
                <input name="contacts[]" class="custom-control-input" type="checkbox" id="{{$contactNoCheck->id}}" value="{{$contactNoCheck->id}}">
                <label class="custom-control-label" for="{{$contactNoCheck->id}}">{{$contactNoCheck->lastname}}</label>
            </div>
        @endforeach
    </div>
@endsection
