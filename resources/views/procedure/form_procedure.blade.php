@extends('procedure.base_form_procedure')

@section('form_procedure')
    <form method="POST" action="{{ 'procedure.edit' === Route::current()->getName() ? route('procedure.update', $procedure->id) : route('procedure.store') }}">
        @csrf
        @if ('procedure.edit' === Route::current()->getName())
            @method('PUT')
        @endif
        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button style="font-size: 0.7em" class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <i class="far fa-file-alt mr-3"></i>Procédure
                        </button>
                    </h2>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="row mb-4">
                            <div class="col"><span class="text-muted">Rédiger ici la procédure</span></div>
                        </div>
                        <div class="form-group row">
                            <label for="title" class="col-md-4 col-form-label text-md-right">Titre</label>
                            <div class="col-md-6">
                                @yield('procedure.title')
                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="category" class="col-md-4 col-form-label text-md-right">Category</label>
                            <div class="col-md-6">
                                <div>
                                    @yield('procedure.category')
                                    <div id="createCategory" style="display: none">
                                        <div class="row mt-4">
                                            <div class="col-md-10">
                                                <input placeholder="Nom de la categorie" id="catName" type="text" class="form-control" name="catName" autofocus>
                                                <span class="invalid-feedback" id="catNameError" role="alert">
                                            <strong></strong>
                                        </span>
                                            </div>
                                            <div class="col-md-2">
                                                <a href="" id="saveNewCategory">
                                                    <i class="far fa-save fa-2x"></i>
                                                </a>
                                                <div class="spinner-border" id="spinnerCategory" style="display: none" role="status">
                                                    <span class="sr-only">Loading...</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @error('category')
                                <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right" for="abstract">Resumé</label>
                            <div class="col-md-6">
                                @yield('procedure.abstract')
                            </div>
                            @error('abstract')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                @yield('procedure.content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <button style="font-size: 0.7em" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <i class="far fa-folder mr-3"></i> Documents
                        </button>
                    </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="row mb-4">
                            <div class="col"><span class="text-muted">Choisir ici les documents de la procédure</span></div>
                        </div>
                        <div class="form-group row" id="documentsCheck">
                            @yield('procedure.documents')
                        </div>
                        <div class="row">
                            <div class="col"><span class="text-primary mr-3">Téléchargeable</span><span class="text-dark">Non Téléchargeable</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                        <button style="font-size: 0.7em" class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <i class="far fa-address-book mr-3"></i>Contact
                        </button>
                    </h2>
                </div>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="row mb-4">
                            <div class="col"><span class="text-muted">Choisir ici les contacts de la procédure</span></div>
                        </div>
                        <div class="row mb-4" id="contacts">
                            @yield('procedure.contact')
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="form-group row mb-0">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn btn-primary">
                                Enregistrer
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('script')
    <script src="https://cdn.tiny.cloud/1/yme02n5ckioeoumbn9mr6hcl1r3u5rn8ubfeqbsyw79quoss/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script src="{{asset('js/procedure.create.edit.js')}}"></script>
@endsection
