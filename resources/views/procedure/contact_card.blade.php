<div id="contactCard" class="modal fade shadow" role="dialog">
    <div class="modal-dialog modal-lg float-left modal-dialog-centered ml-4 modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="row p-3">
                <div class="col"><h5 class="modal-title w-100 text-center font-weight-bold text-primary" id="fullName"></h5></div>
                <div class="col-1">
                    <button type="button" class="close float-right text-primary" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="fa fa-times"></span>
                    </button>
                </div>
            </div>
            <div class="row mt-0">
                <div class="col">
                    <div id="contact" class="modal-body col-12 pt-0">
                        <div class="col-md">
                            <div class="contact-box center-version">
                                <strong id="company"></strong><br>
                                <address class="m-t-md" id="address">
                                </address>
                                <div id="phones" class="mb-3"></div>
                                <a id="email" href="" class="mb-1"><span></span></a>
                                <div id="comments" class="font-italic my-4"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

