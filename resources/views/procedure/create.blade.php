@extends('procedure.form_procedure')

@section('procedure.title')
    <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" autocomplete="title" autofocus>
@endsection

@section('procedure.category')
    <select id="category" class="form-control @error('category') is-invalid @enderror" value="{{old('category')}}" name="category">
        <option></option>
        <option id="addNewCat">Ajouter une nouvelle categorie</option>
        @foreach($categories as $category)
            <option value="{{$category->id}}" {{ (old('category') == $category->id) ? 'selected' : '' }}>{{$category->name}}</option>
        @endforeach
    </select>
@endsection

@section('procedure.abstract')
    <textarea class="form-control" id="abstract" name="abstract" placeholder="Court resumé de la procedure" rows="3"></textarea>
@endsection

@section('procedure.content')
    <input type="text" name="contenu" id="editor" />
@endsection

@section('procedure.documents')
    @if ($documents->count() > 0)
        @foreach($documents->chunk($documents->count()/2 > 1 ? $documents->count()/2 : $documents->count()) as $chunk)
            <div class="col-6 mb-1">
                @foreach($chunk as $document)
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" name="documents[]" type="checkbox" id="document_{{$document->id}}" value="{{$document->id}}">
                        <label class="custom-control-label {{$document->path ? 'text-primary' : ''}}" for="document_{{$document->id}}">
                            {{$document->name}}
                        </label>
                    </div>
                @endforeach
            </div>
        @endforeach
    @else
        <div class="col-12 mb-1">Pas encore de document...</div>
    @endif
@endsection

@section('procedure.contact')
    @if ($contacts->count() > 0)
        @foreach($contacts->chunk($contacts->count()/2 > 1 ? $contacts->count()/2 : $contacts->count()) as $chunk)
            <div class="col-6">
                @foreach($chunk as $contact)
                    <div class="custom-control custom-checkbox">
                        <input class="custom-control-input" name="contacts[]" type="checkbox" id="{{$contact->id}}" value="{{$contact->id}}">
                        <label class="custom-control-label" for="{{$contact->id}}">
                            {{$contact->lastname}} {{$contact->firstname}}
                        </label>
                    </div>
                @endforeach
            </div>
        @endforeach
    @else
        <div class="col-12 mb-1">Pas encore de contact...</div>
    @endif
@endsection
