@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 mb-5">
            <div class="input-group input-group-lg">
                <input type="text" id="searchInput" class="form-control" placeholder="Rechercher un titre, description etc..." aria-label="Sizing example input" aria-describedby="inputGroup-sizing-lg">
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div id="spinner" class="col text-center" style="display: none">
                <div class="spinner-border text-primary" role="status">
                    <span class="sr-only">Loading...</span>
                </div>
            </div>
            <div class="card-deck" id="listProcedure">
            </div>
        </div>
    </div>
</div>
<!-- Modale -->
<div id="procedureShow" class="modal fade shadow" role="dialog">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header d-block">
                <div class="row">
                    <div class="col-3 align-middle">
                        <button id="showDocuments" type="button" class="close float-left text-primary" data-toggle="tooltip"  data-placement="bottom" title="Liste des documents">
                            <span aria-hidden="true" class="fas fa-folder"></span>
                        </button>
                        <button id="showContacts" type="button" class="close float-left text-primary" data-toggle="tooltip"  data-placement="bottom" title="Liste des contacts">
                            <span aria-hidden="true" class="fas fa-address-book"></span>
                        </button>
                        <button id="editProcedure" type="button" class="close float-left text-primary" data-toggle="tooltip"  data-placement="bottom" title="Editer la procédure">
                            <span aria-hidden="true" class="fas fa-edit"></span>
                        </button>
                        <form id="formDelete" action="" method="POST">
                            @csrf
                            @method('DELETE')
                            <button id="deleteProcedure" type="button" class="close float-left text-primary" data-toggle="tooltip"  data-placement="bottom" title="Supprimer la procédure">
                                <span aria-hidden="true" class="fas fa-trash"></span>
                            </button>
                        </form>
                    </div>
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <h5 class="modal-title w-100 text-center font-weight-bold text-primary" id="titleProcedure"></h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <h6 class="card-subtitle text-center text-muted text-danger font-italic mt-1" id="category"></h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-1">
                        <button type="button" class="close float-right text-primary" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" class="fa fa-times"></span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div id="contacts">
                    <div class="col well" id="contactsList" style="display: none">
                        <h5 class="p-2">Contacts</h5>
                        <ul></ul>
                    </div>
                    <div class="col well" id="noContacts" style="display: none">
                        <p class="p-2">Aucun contact..</p>
                    </div>
                </div>
                <div class="col well border-left">
                    <div id="documents">
                        <div id="noDocument" class="modal-body border-bottom col-12 font-italic" style="display: none">Aucun document pour cette procédure...</div>
                        <div id="documentsList" class="modal-body border-bottom col-12" style="display: none">
                            <h5 class="pl-2">Documents</h5>
                            <div class="row my-3">
                                <div class="col-6">
                                    <ul id="docDownload" class="list-group"></ul>
                                </div>
                                <div class="col-6">
                                    <ul id="docNoDownload" class="list-group"></ul>
                                </div>
                            </div>
                            <small><span class="text-primary mr-3">Téléchargeable</span><span class="text-dark">Non Téléchargeable</span></small>
                        </div>
                    </div>
                    <div id="procedure" class="modal-body col-12">
                        <div class="col-12 well" id="bodyProcedure">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('procedure.contact_card')
@endsection
@section('script')
    <script src="{{asset('js/procedure.index.js')}}"></script>
@endsection
