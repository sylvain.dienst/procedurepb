<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Auth::routes();

Route::middleware('auth')->group(function () {
    /* Home */
    Route::get('/', 'HomeController@index')->name('home');

    /* Procedure */
    Route::prefix('procedure')->group(function () {
        Route::name('procedure.')->group(function () {
            Route::get('', 'ProcedureController@index')->name('index');
            Route::get('create', 'ProcedureController@create')->name('create');
            Route::post('', 'ProcedureController@store')->name('store');
            Route::get('{id}/edit', 'ProcedureController@edit')->name('edit');
            Route::put('{id}', 'ProcedureController@update')->name('update');
            Route::delete('{id}', 'ProcedureController@destroy')->name('destroy');
            Route::get('{id}/documents', 'ProcedureController@showDocuments')->name('showDocuments');
            Route::get('{id}/contacts', 'ProcedureController@showContacts')->name('showContacts');
            Route::get('search/{query}', 'ProcedureController@search')->name('search');
        });
    });

    /* Category */
    Route::prefix('category')->group(function () {
        Route::name('category.')->group(function () {
            Route::post('', 'CategoryController@store')->name('store');
            Route::put('{id}', 'CategoryController@update')->name('update');
            Route::delete('{id}', 'CategoryController@destroy')->name('destroy');
        });
    });

    /* Document */
    Route::prefix('document')->group(function () {
        Route::name('document.')->group(function () {
            Route::post('', 'DocumentController@store')->name('store');
            Route::post('{id}/file', 'DocumentController@storeFile')->name('store.file');
            Route::get('{id}/file', 'DocumentController@downloadFile')->name('download.file');
            Route::put('{id}', 'ProcedureController@update')->name('update');
            Route::delete('{id}', 'ProcedureController@destroy')->name('destroy');
        });
    });

    /* Contact */
    Route::prefix('contact')->group(function () {
        Route::name('contact.')->group(function () {
            Route::post('', 'ContactController@store')->name('store');
            Route::put('{id}', 'ContactController@update')->name('update');
            Route::delete('{id}', 'ContactController@destroy')->name('destroy');
            Route::get('{id}/phones', 'ContactController@showPhones')->name('showPhones');
        });
    });

    /*Phone*/
    Route::prefix('phone')->group(function () {
        Route::name('phone.')->group(function () {
            Route::post('', 'PhoneController@store')->name('store');
            Route::put('{id}', 'PhoneController@update')->name('update');
            Route::delete('{id}', 'PhoneController@destroy')->name('destroy');
        });
    });
});
